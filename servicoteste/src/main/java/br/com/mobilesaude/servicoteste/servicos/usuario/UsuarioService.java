package br.com.mobilesaude.servicoteste.servicos.usuario;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.mobilesaude.servicoteste.bean.ExtratoUtilizacaoUsuarioBean;
import br.com.mobilesaude.servicoteste.bean.UsuarioBean;
import br.com.mobilesaude.servicoteste.exceptions.BusinessException;
import br.com.mobilesaude.servicoteste.model.DadosUsuarioTO;
import br.com.mobilesaude.servicoteste.model.ExtratoUtilizacaoTO;
import br.com.mobilesaude.servicoteste.model.ParamUsuario;
import br.com.mobilesaude.servicoteste.model.UsuarioTO;
import br.com.mobilesaude.servicoteste.retorno.DadosUsuario;
import br.com.mobilesaude.servicoteste.retorno.ExtratoUtilizacao;
import br.com.mobilesaude.servicoteste.retorno.RetornoDadosUsuario;
import br.com.mobilesaude.servicoteste.retorno.RetornoPadrao;
import br.com.mobilesaude.servicoteste.retorno.RetornoUsuario;
import br.com.mobilesaude.servicoteste.retorno.Usuario;

@Path("usuario/v1")
public class UsuarioService {

	@Inject
	private UsuarioBean usuarioBean;

	@Context
	private HttpServletRequest request;
	
	@Inject
	private ExtratoUtilizacaoUsuarioBean extratoUtilizacaoUsuarioBean;

	public static final String ERRO_LOGIN = "Usuário não encontrado com as informações repassadas.";
	public static final String ERRO_DETALHE_USUARIO = "Usuário não encontrado com as informações repassadas.";

	@POST
	@Path("/retornaUsuario")
	@Produces("application/json")
	public RetornoUsuario getRetornaUsuario(ParamUsuario paramUsuario) {
		try {
			if (paramUsuario == null || paramUsuario.getUsuario().isEmpty() || paramUsuario.getSenha().isEmpty()) {
				return (RetornoUsuario) new RetornoUsuario().setError("Usuário ou senha inválidos.");
			}

			RetornoUsuario retornoUsuario = new RetornoUsuario();

			UsuarioTO usuarioTO = usuarioBean.retornaUsuario(paramUsuario);
			if (usuarioTO == null) {
				retornoUsuario.setStatus(false);
				retornoUsuario.setError(ERRO_LOGIN);
				retornoUsuario.setUsuario(null);
				return retornoUsuario;
			}

			// Preencher objetos de retorno para montar o JSON
			Usuario usuarioRetorno = new Usuario();
			usuarioRetorno.setId(usuarioTO.getId().toString());
			usuarioRetorno.setUsuario(usuarioTO.getUsuario());
			usuarioRetorno.setSenha(usuarioTO.getSenha());
			retornoUsuario.setStatus(true);
			retornoUsuario.setUsuario(usuarioRetorno);

			return retornoUsuario;
		} catch (Exception e) {
			e.printStackTrace();
			return (RetornoUsuario) new RetornoUsuario().setError("Sistema indisponível");
		}
	}
	
	@GET
	@Path("/retornaDadosDoUsuario/{idUsuario}")
	@Produces("application/json")
	public RetornoDadosUsuario getRetornaDadosDoUsuario(@PathParam("idUsuario") Long idUsuario) {
		try {

			RetornoDadosUsuario retornoDadosUsuario = new RetornoDadosUsuario();

			DadosUsuarioTO dadosUsuarioTO = usuarioBean.retornaDadosDoUsuario(idUsuario);
			if (dadosUsuarioTO == null) {
				retornoDadosUsuario.setStatus(false);
				retornoDadosUsuario.setError(ERRO_DETALHE_USUARIO);
				retornoDadosUsuario.setUsuario(null);
				retornoDadosUsuario.setDadosUsuario(null);
				return retornoDadosUsuario;
			}

			// Preencher objetos de retorno para montar o JSON
			DadosUsuario dadosUsuarioRetorno = new DadosUsuario();
			
			Usuario usuarioRetorno = new Usuario();
			
			UsuarioTO usuario = dadosUsuarioTO.getUsuario();
			usuarioRetorno.setId(usuario.getId().toString());
			usuarioRetorno.setUsuario(usuario.getUsuario());
			
			dadosUsuarioRetorno.setIdUsuario(usuario.getId().toString());
			dadosUsuarioRetorno.setNome(dadosUsuarioTO.getNome());
			dadosUsuarioRetorno.setSobrenome(dadosUsuarioTO.getSobreNome());
			
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			String dataNascimento = fmt.format(dadosUsuarioTO.getDataNascimento());
			dadosUsuarioRetorno.setDataNascimento(dataNascimento);
			
			dadosUsuarioRetorno.setTelefone(dadosUsuarioTO.getTelefone());
			dadosUsuarioRetorno.setEmail(dadosUsuarioTO.getEmail());
			
			retornoDadosUsuario.setStatus(true);
			retornoDadosUsuario.setUsuario(usuarioRetorno);
			retornoDadosUsuario.setDadosUsuario(dadosUsuarioRetorno);

			return retornoDadosUsuario;
		} catch (Exception e) {
			e.printStackTrace();
			return (RetornoDadosUsuario) new RetornoDadosUsuario().setError("Sistema indisponível");
		}
	}
	
	
	@GET
	@Path("/extrato-utilizacao/{idUsuario}")
	@Produces("application/json")
	public Response getExtratoUtilizacao(@PathParam("idUsuario") Long idUsuario,
			@QueryParam("anoCompetencia") Integer anoCompetencia,
			@QueryParam("mesCompetencia") Integer mesCompetencia) {

		try {

			List<ExtratoUtilizacaoTO> extratosUtilizacaoTO = extratoUtilizacaoUsuarioBean.retornaExtratosUtilizacao(idUsuario, anoCompetencia, mesCompetencia);
			
			List<ExtratoUtilizacao> extratosUtilizacao = new ArrayList<ExtratoUtilizacao>();
			
			extratosUtilizacaoTO.forEach( extratoUtilizacaoTO -> {
				extratosUtilizacao.add( toExtratoUtilizacao(extratoUtilizacaoTO) );
			});
			
			return Response.ok().entity(extratosUtilizacao).build();

		} catch (Exception e) {
			e.printStackTrace();

			if (e instanceof BusinessException) {
				return Response.status(Status.BAD_REQUEST).entity(new RetornoPadrao().setError(e.getMessage())).build();

			} else {
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new RetornoPadrao().setError(e.getMessage())).build();
			}

		}
	}
	
	
	private ExtratoUtilizacao toExtratoUtilizacao(ExtratoUtilizacaoTO extratoUtilizacaoTO) {
		
		ExtratoUtilizacao extratoUtilizacao = new ExtratoUtilizacao(); 
		
		extratoUtilizacao.setAno( extratoUtilizacaoTO.getAno() );
		extratoUtilizacao.setCodigoProcedimento( extratoUtilizacaoTO.getCodigoProcedimento() );
		extratoUtilizacao.setMes( extratoUtilizacaoTO.getMes() );
		extratoUtilizacao.setNomeProcedimento( extratoUtilizacaoTO.getNomeProcedimento() );
		extratoUtilizacao.setQuantidadeAutorizada( extratoUtilizacaoTO.getQuantidadeAutorizada() );
		extratoUtilizacao.setValor( extratoUtilizacaoTO.getValor() );
		
		return extratoUtilizacao;
		
	}

}
