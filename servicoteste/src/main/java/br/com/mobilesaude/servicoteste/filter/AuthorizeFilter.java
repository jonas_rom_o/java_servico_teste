package br.com.mobilesaude.servicoteste.filter;

import java.io.IOException;
import java.util.Base64;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import br.com.mobilesaude.servicoteste.bean.UsuarioBean;
import br.com.mobilesaude.servicoteste.model.ParamUsuario;
import br.com.mobilesaude.servicoteste.model.UsuarioTO;

@Priority( Priorities.AUTHENTICATION )
@PreMatching
@Provider
public class AuthorizeFilter implements ContainerRequestFilter {

	@Inject
	private UsuarioBean usuarioBean;

	@Override
	public void filter( ContainerRequestContext requestContext ) throws IOException {
		String authHeaderVal = requestContext.getHeaderString( "Authorization" );

		if( authHeaderVal != null && authHeaderVal.startsWith( "Basic" ) ) {
			try {

				byte[] result = Base64.getDecoder().decode( authHeaderVal.split( " " )[1] );

				String usuarioSenha = new String( result );

				String usuarioSenhaArr[] = usuarioSenha.split( ":" );
				ParamUsuario paramUsuario = new ParamUsuario( usuarioSenhaArr[0], usuarioSenhaArr[1] );

				UsuarioTO usuario = usuarioBean.retornaUsuario( paramUsuario );

				if( usuario != null ) {
					requestContext.setProperty( "usuario", usuario );
				} else {
					requestContext.abortWith( Response.status( Response.Status.UNAUTHORIZED ).build() );
				}

			} catch ( Exception ex ) {
				requestContext.abortWith( Response.status( Response.Status.UNAUTHORIZED ).build() );
			}
		} else {
			requestContext.abortWith( Response.status( Response.Status.UNAUTHORIZED ).build() );
		}
	}

}
