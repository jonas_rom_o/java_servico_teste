package br.com.mobilesaude.servicoteste.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table( name = "log_acesso" )
@Entity( name = "LogAcesso" )
public class LogAcessoTO {

	@Id
	@Column( name = "id" )
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	@Column( name = "data_request" )
	private LocalDateTime dataRequest;

	@Column( name = "endpoint" )
	private String endpoint;

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "id_usuario" )
	private UsuarioTO usuario;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public LocalDateTime getDataRequest() {
		return dataRequest;
	}

	public void setDataRequest( LocalDateTime dataRequest ) {
		this.dataRequest = dataRequest;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint( String endpoint ) {
		this.endpoint = endpoint;
	}

	public UsuarioTO getUsuario() {
		return usuario;
	}

	public void setUsuario( UsuarioTO usuario ) {
		this.usuario = usuario;
	}

}
