package br.com.mobilesaude.servicoteste.bean;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.mobilesaude.servicoteste.dao.LogAcessoDAO;
import br.com.mobilesaude.servicoteste.exceptions.BusinessException;
import br.com.mobilesaude.servicoteste.model.LogAcessoTO;
import br.com.mobilesaude.servicoteste.model.UsuarioTO;

@Stateless
public class LogAcessoBean {

	@EJB
	private LogAcessoDAO logAcessoDAO;
	
	@EJB
	private UsuarioBean usuarioBean;
	
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

	/**
	 * Cria e atualiza um LogAcessoTO, retorna uma entidade gerenciada
	 * 
	 * @param LogAcessoTO
	 * @return LogAcessoTO
	 * @throws BusinessException
	 */
	public LogAcessoTO merge( LogAcessoTO logAcessoTO ) throws BusinessException {

		return logAcessoDAO.merge( logAcessoTO );

	}
	
	
	/**
	 * Retorna LogAcessoTO de acordo com os parametros informados
	 * 
	 * @param Long idUsuario
	 * @param String endpoint
	 * @param String dataInicioStr
	 * @param String dataFimStr
	 * @return LogAcessoTO
	 * @throws BusinessException
	 */
	public List<LogAcessoTO> retornarLogsAcesso(Long idUsuario, String endpoint, String dataInicioStr, String dataFimStr) throws BusinessException {
		
		LocalDateTime dataInicio = toLocalDateTime(dataInicioStr);
		LocalDateTime dataFim = toLocalDateTime(dataFimStr);
		return logAcessoDAO.retornaLogAcesso(idUsuario, endpoint, dataInicio, dataFim);
	}
	
	public void registrarLogAcesso(Long idUsuario, String endpoint ) throws BusinessException {
		UsuarioTO usuarioTO = usuarioBean.retornaUsuario( idUsuario );
		if(usuarioTO == null) {
			throw new BusinessException( "Usuário inválido" );
		}
		
		LogAcessoTO log = new LogAcessoTO();
		log.setUsuario( usuarioTO );
		log.setEndpoint( endpoint );
		log.setDataRequest( LocalDateTime.now() );
		
		merge( log );
		
	}
	
	private LocalDateTime toLocalDateTime(String dataStr) throws BusinessException {
		try {
			if(dataStr == null || dataStr.isEmpty()) {
				return null;
			} else {
				return LocalDateTime.parse(dataStr, formatter);
			}			
		} catch (Exception e) {
			throw new BusinessException("Data inválida");
		}
	}

}
