package br.com.mobilesaude.servicoteste.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.mobilesaude.servicoteste.model.ExtratoUtilizacaoTO;

@Stateless
public class ExtratoUtilizacaoUsuarioDAO {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Retorna ExtratoUtilizacao de acordo com parametros
	 * 
	 * @param idUsuario
	 * @return List<ExtratoUtilizacaoTO>
	 */
	public List<ExtratoUtilizacaoTO> retornaExtratosUtilizacao( Long idUsuario, Integer anoCompetencia, Integer mesCompetencia ) {
		em.clear();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append( "select e from ExtratoUtilizacao e inner join e.usuario u where 1 = 1" );
		strQuery.append( " and u.id = :idUsuario " );

		if( anoCompetencia != null ) {
			strQuery.append( " and e.ano = :anoCompetencia " );
		}

		if( mesCompetencia != null ) {
			strQuery.append( " and e.mes = :mesCompetencia " );
		}

		TypedQuery<ExtratoUtilizacaoTO> query = em.createQuery( strQuery.toString(), ExtratoUtilizacaoTO.class );
		query.setParameter( "idUsuario", idUsuario );
		if( anoCompetencia != null ) {
			query.setParameter( "anoCompetencia", anoCompetencia );
		}

		if( mesCompetencia != null ) {
			query.setParameter( "mesCompetencia", mesCompetencia );
		}

		return query.getResultList();
	}
}
