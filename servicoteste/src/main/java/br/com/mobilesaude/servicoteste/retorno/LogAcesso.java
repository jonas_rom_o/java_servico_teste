package br.com.mobilesaude.servicoteste.retorno;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LogAcesso {

	@JsonProperty("usuario")
	private String usuario;

	@JsonProperty("dataRequest")
	private String dataRequest;

	@JsonProperty("endpoint")
	private String endpoint;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getDataRequest() {
		return dataRequest;
	}

	public void setDataRequest(String dataRequest) {
		this.dataRequest = dataRequest;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

}
