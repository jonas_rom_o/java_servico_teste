package br.com.mobilesaude.servicoteste.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table( name = "extrato_utilizacao" )
@Entity( name = "ExtratoUtilizacao" )
public class ExtratoUtilizacaoTO {

	@Id
	@Column( name = "id" )
	private Long id;

	@Column( name = "codigo_procedimento" )
	private String codigoProcedimento;

	@Column( name = "nome_procedimento" )
	private String nomeProcedimento;

	@Column( name = "quantidade_autorizada" )
	private int quantidadeAutorizada;

	@Column( name = "ano" )
	private int ano;

	@Column( name = "mes" )
	private int mes;

	@Column( name = "valor" )
	private int valor;

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "id_usuario" )
	private UsuarioTO usuario;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getCodigoProcedimento() {
		return codigoProcedimento;
	}

	public void setCodigoProcedimento( String codigoProcedimento ) {
		this.codigoProcedimento = codigoProcedimento;
	}

	public String getNomeProcedimento() {
		return nomeProcedimento;
	}

	public void setNomeProcedimento( String nomeProcedimento ) {
		this.nomeProcedimento = nomeProcedimento;
	}

	public int getQuantidadeAutorizada() {
		return quantidadeAutorizada;
	}

	public void setQuantidadeAutorizada( int quantidadeAutorizada ) {
		this.quantidadeAutorizada = quantidadeAutorizada;
	}

	public int getAno() {
		return ano;
	}

	public void setAno( int ano ) {
		this.ano = ano;
	}

	public int getMes() {
		return mes;
	}

	public void setMes( int mes ) {
		this.mes = mes;
	}

	public int getValor() {
		return valor;
	}

	public void setValor( int valor ) {
		this.valor = valor;
	}

	public UsuarioTO getUsuario() {
		return usuario;
	}

	public void setUsuario( UsuarioTO usuario ) {
		this.usuario = usuario;
	}

}
