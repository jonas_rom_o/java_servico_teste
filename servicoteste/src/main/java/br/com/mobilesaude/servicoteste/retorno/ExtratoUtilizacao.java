package br.com.mobilesaude.servicoteste.retorno;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExtratoUtilizacao {

	@JsonProperty( "codigoProcedimento" )
	private String codigoProcedimento;

	@JsonProperty( "nomeProcedimento" )
	private String nomeProcedimento;

	@JsonProperty( "quantidadeAutorizada" )
	private int quantidadeAutorizada;

	@JsonProperty( "ano" )
	private int ano;

	@JsonProperty( "mes" )
	private int mes;

	@JsonProperty( "valor" )
	private int valor;

	public String getCodigoProcedimento() {
		return codigoProcedimento;
	}

	public void setCodigoProcedimento( String codigoProcedimento ) {
		this.codigoProcedimento = codigoProcedimento;
	}

	public String getNomeProcedimento() {
		return nomeProcedimento;
	}

	public void setNomeProcedimento( String nomeProcedimento ) {
		this.nomeProcedimento = nomeProcedimento;
	}

	public int getQuantidadeAutorizada() {
		return quantidadeAutorizada;
	}

	public void setQuantidadeAutorizada( int quantidadeAutorizada ) {
		this.quantidadeAutorizada = quantidadeAutorizada;
	}

	public int getAno() {
		return ano;
	}

	public void setAno( int ano ) {
		this.ano = ano;
	}

	public int getMes() {
		return mes;
	}

	public void setMes( int mes ) {
		this.mes = mes;
	}

	public int getValor() {
		return valor;
	}

	public void setValor( int valor ) {
		this.valor = valor;
	}

}
