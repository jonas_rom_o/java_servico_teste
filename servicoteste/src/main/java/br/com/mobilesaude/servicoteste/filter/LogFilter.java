package br.com.mobilesaude.servicoteste.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import br.com.mobilesaude.servicoteste.bean.LogAcessoBean;
import br.com.mobilesaude.servicoteste.exceptions.BusinessException;
import br.com.mobilesaude.servicoteste.model.UsuarioTO;

@Provider
public class LogFilter implements ContainerRequestFilter {

	@Inject
	private LogAcessoBean logAcessoBean;

	@Override
	public void filter( ContainerRequestContext requestContext ) throws IOException {

		try {
			UsuarioTO usuario = ( UsuarioTO ) requestContext.getProperty( "usuario" );
			logAcessoBean.registrarLogAcesso( usuario.getId(), requestContext.getUriInfo().getPath() );
		} catch ( BusinessException e ) {
			e.printStackTrace();
		}

	}

}
