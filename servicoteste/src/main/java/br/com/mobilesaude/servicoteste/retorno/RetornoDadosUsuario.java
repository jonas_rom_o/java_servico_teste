package br.com.mobilesaude.servicoteste.retorno;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RetornoDadosUsuario extends RetornoPadrao {

	@JsonProperty("usuario")
	private Usuario usuario;
	
	@JsonProperty("dadosUsuario")
	private DadosUsuario dadosUsuario;

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public DadosUsuario getDadosUsuario() {
		return dadosUsuario;
	}

	public void setDadosUsuario(DadosUsuario dadosUsuario) {
		this.dadosUsuario = dadosUsuario;
	}
	
}
