package br.com.mobilesaude.servicoteste.bean;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.mobilesaude.servicoteste.dao.ExtratoUtilizacaoUsuarioDAO;
import br.com.mobilesaude.servicoteste.exceptions.BusinessException;
import br.com.mobilesaude.servicoteste.model.ExtratoUtilizacaoTO;

@Stateless
public class ExtratoUtilizacaoUsuarioBean {

	@EJB
	private ExtratoUtilizacaoUsuarioDAO extratoUtilizacaoUsuarioDAO;
	
	
	/**
	 * Retorna ExtratoUtilizacao de acordo com parametros informados
	 * 
	 * @param idUsuario
	 * @param anoCompetencia
	 * @param mesCompetencia
	 * @return List<ExtratoUtilizacao>
	 * @throws BusinessException 
	 */
	public List<ExtratoUtilizacaoTO> retornaExtratosUtilizacao(Long idUsuario, Integer anoCompetencia, Integer mesCompetencia) throws BusinessException 
    {
       
		validaMesCompetencia(mesCompetencia);
		
		List<ExtratoUtilizacaoTO> extratosUtilizacaoTO = extratoUtilizacaoUsuarioDAO.retornaExtratosUtilizacao( idUsuario, anoCompetencia, mesCompetencia );
		
		return extratosUtilizacaoTO;
    }
	
	private void validaMesCompetencia(Integer mesCompetencia) throws BusinessException {
		
		if(mesCompetencia == null) {
			return;
		}
		
		if(mesCompetencia < 1 || mesCompetencia > 12) {
			throw new BusinessException( "Mês competencia inválido" );
		}
		
	}
}
