package br.com.mobilesaude.servicoteste.dao;

import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.mobilesaude.servicoteste.model.LogAcessoTO;

@Stateless
public class LogAcessoDAO {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Retorna ExtratoUtilizacao de acordo com parametros
	 * 
	 * @param idUsuario
	 * @return List<ExtratoUtilizacaoTO>
	 */
	public List<LogAcessoTO> retornaLogAcesso( Long idUsuario, String endpoint, LocalDateTime dataInicio, LocalDateTime dataFim ) {
		em.clear();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append( "select l from LogAcesso l inner join fetch l.usuario u where 1 = 1" );

		if( idUsuario != null ) {
			strQuery.append( " and u.id = :idUsuario " );
		}

		if( endpoint != null && !endpoint.trim().isEmpty()) {
			strQuery.append( " and l.endpoint like :endpoint " );
		}

		if( dataInicio != null && dataFim != null ) {
			strQuery.append( " and l.dataRequest between :dataInicio and :dataFim " );
		}

		TypedQuery<LogAcessoTO> query = em.createQuery( strQuery.toString(), LogAcessoTO.class );
		if( idUsuario != null ) {
			query.setParameter( "idUsuario", idUsuario );
		}

		if( endpoint != null && !endpoint.trim().isEmpty()) {
			query.setParameter( "endpoint", "%"+endpoint+"%" );
		}

		if( dataInicio != null && dataFim != null ) {
			query.setParameter( "dataInicio", dataInicio );
			query.setParameter( "dataFim", dataFim );
		}

		return query.getResultList();
	}

	/**
	 * Cria e atualiza um LogAcessoTO, retorna uma entidade gerenciada 
	 * 
	 * @param LogAcessoTO logAcessoTO
	 * @return LogAcessoTO
	 */
	public LogAcessoTO merge( LogAcessoTO logAcessoTO ) {
		em.clear();

		return em.merge( logAcessoTO );
	}
}
