package br.com.mobilesaude.servicoteste.servicos;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.mobilesaude.servicoteste.bean.LogAcessoBean;
import br.com.mobilesaude.servicoteste.exceptions.BusinessException;
import br.com.mobilesaude.servicoteste.model.LogAcessoTO;
import br.com.mobilesaude.servicoteste.retorno.LogAcesso;
import br.com.mobilesaude.servicoteste.retorno.RetornoPadrao;

@Path("log-acesso/v1")
public class LogAcessoService {

	@Inject
	private LogAcessoBean logAcessoBean;
	
	@GET
	@Path("/")
	@Produces("application/json")
	public Response getExtratoUtilizacao(@QueryParam("idUsuario") Long idUsuario, @QueryParam("endpoint") String endpoint, @QueryParam("dataInicio") String dataInicio, @QueryParam("dataFim") String dataFim) {

		try {

			List<LogAcessoTO> logsTO = logAcessoBean.retornarLogsAcesso(idUsuario, endpoint, dataInicio, dataFim);
			
			List<LogAcesso> logs = new ArrayList<LogAcesso>(); 
			
			logsTO.forEach(logTO -> {
				logs.add(toLogAcesso(logTO));
			});
			
			return Response.ok().entity(logs).build();

		} catch (Exception e) {
			e.printStackTrace();
			
			if (e instanceof BusinessException) {
				return Response.status(Status.BAD_REQUEST).entity(new RetornoPadrao().setError(e.getMessage())).build();

			} else {
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new RetornoPadrao().setError(e.getMessage())).build();
			}

		}
	}
	
	public LogAcesso toLogAcesso(LogAcessoTO logAcessoTO) {
		
		LogAcesso logAcesso = new LogAcesso();
		
		logAcesso.setDataRequest( logAcessoTO.getDataRequest().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
		logAcesso.setEndpoint(logAcessoTO.getEndpoint());
		logAcesso.setUsuario(logAcessoTO.getUsuario().getUsuario());
		
		return logAcesso;
		
	}
	
	
}
