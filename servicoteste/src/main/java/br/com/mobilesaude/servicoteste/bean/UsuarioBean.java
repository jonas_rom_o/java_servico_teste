package br.com.mobilesaude.servicoteste.bean;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.mobilesaude.servicoteste.model.DadosUsuarioTO;
import br.com.mobilesaude.servicoteste.model.ParamUsuario;
import br.com.mobilesaude.servicoteste.model.UsuarioTO;


@Stateless
public class UsuarioBean {

	@PersistenceContext
	private EntityManager em;
	
	
	/**
	 * Passa o usuario e senha para retornar o id do usuario a ser repassado no proximo endpoint que retorna os detalhes
	 * 
	 * @param paramUsuario
	 * @return UsuarioTO
	 */
	public UsuarioTO retornaUsuario(ParamUsuario paramUsuario) 
    {
        em.clear();
        Query query = em.createNativeQuery("select id, usuario, senha from usuario where usuario = ? and senha = ?", UsuarioTO.class);
        query.setParameter(1, paramUsuario.getUsuario());
	    query.setParameter(2, paramUsuario.getSenha());
        try {
        	return (UsuarioTO) query.getSingleResult();			
		} catch (NoResultException e) {
			return null;
		}
    }
	
	/**
	 * Retorna os detalhes do usuario
	 * 
	 * @param idUsuario
	 * @return UsuarioTO
	 */
	public DadosUsuarioTO retornaDadosDoUsuario(Long idUsuario) 
    {
        em.clear();
        Query query = em.createNativeQuery("select * from dados_usuario where id_usuario = ?", DadosUsuarioTO.class);
        query.setParameter(1, idUsuario);
        if (query.getResultList().size() > 0)
        	return (DadosUsuarioTO) query.getResultList().get(0);
        
        return null;
    }
	
	/**
	 * Retorna usuario filtrando por id
	 * 
	 * @param idUsuario
	 * @return UsuarioTO
	 */
	public UsuarioTO retornaUsuario( Long idUsuario ) {
		em.clear();
		return em.find( UsuarioTO.class, idUsuario );
	}

}

