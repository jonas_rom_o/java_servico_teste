package br.com.mobilesaude.servicoteste.model;

public class ParamUsuario {

	private String usuario;
	private String senha;
	
	public ParamUsuario() {
		// TODO Auto-generated constructor stub
	}
	
	public ParamUsuario(String usuario, String senha) {
		this.usuario = usuario;
		this.senha = senha;
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
}
