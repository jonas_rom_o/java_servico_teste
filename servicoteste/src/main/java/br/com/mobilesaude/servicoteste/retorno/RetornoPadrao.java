package br.com.mobilesaude.servicoteste.retorno;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RetornoPadrao {

    @JsonProperty("status")
    protected Boolean status = true;
    @JsonProperty("critica")
    protected String critica;

    @JsonProperty("motivoCritica")
    protected String motivoCritica;

    public RetornoPadrao() {
        super();
    }

    public RetornoPadrao setError(String critica) {
        status = false;
        this.critica = critica;
        this.motivoCritica = critica;
        return this;
    }

    public RetornoPadrao setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getCritica() {
        return critica;
    }

    public String getMotivoCritica() {
        return motivoCritica;
    }
}